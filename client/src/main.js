import Vue from 'vue'
import Chartkick from 'chartkick'
import VueChartkick from 'vue-chartkick'
// eslint-disable-next-line
import Chart from 'chart.js'
import App from './App'

Vue.config.productionTip = false
Vue.use(VueChartkick, { Chartkick })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
