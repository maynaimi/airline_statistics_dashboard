import Api from '@/services/Api'

export default {
  getAirlines () {
    return Api().get('airlines')
  },

  getAllAirlinesStats () {
    return Api().get('airline_stats')
  },

  getAirlineStats (airline) {
    return Api().get('airline_stats/' + airline)
  },

  getMeasureStats (measure) {
    return Api().get('measure/' + measure)
  }
}
