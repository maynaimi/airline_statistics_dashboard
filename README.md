# Airline Statistics Dashboard

Solution to Stocard's Flight case (Part 2) - Airline Statistics Dashboard
This application is built using Express.js with Node for the server side and Vue.js for the client side.

![dashboard_screenshot](https://gitlab.com/maynaimi/airline_statistics_dashboard/raw/master/client/static/dashboard_screenshot_2018-08-21.png)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

You will need to clone this repository.

And you will need Node and npm installed. For both the server and client you will need to run:

```
npm install 
```

## Running the tests

### Unit tests

TODO!

### Coding style tests

The application is using the Standard.js styling standard. We are running eslint to test when starting up.

```
"./node_modules/.bin/eslint \"**/*.js\""
```

## Deployment

TODO!

## Built With

* [Node.js](https://nodejs.org/en/) - Javascript Runtime Environment
* [Express.js](https://expressjs.com/) - JS Framework for server side
* [Vue.js](https://vuejs.org/) - JS Framework for client side
* [ChartKick.js](https://chartkick.com/) - JS library for building charts
* [Standard.js](https://standardjs.com/) - JavaScript Standard Style

## Authors

* May Naimi-Jones

## Acknowledgments

* Thanks to my husband for helping me with my many questions :)