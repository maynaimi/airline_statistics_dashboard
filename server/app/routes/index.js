const dashboardRoutes = require('./dashboard')
const airlineStatsRoutes = require('./airline_stats')

module.exports = (app, router) => {
  dashboardRoutes(app)
  airlineStatsRoutes(router)
}
