const airlineStats = require('../../data/airline_statistics')

module.exports = (router) => {
  router.get('/airlines', (req, res) => {
    let airlines = airlineStats.map((val, idx, arr) => {
      return val.airlineCode
    })
    res.json(airlines)
  })

  router.get('/airline_stats/:airline', (req, res) => {
    let oneAirlineStats = airlineStats.filter((obj) => {
      return obj.airlineCode === req.params.airline
    })[0] // We've assumed there will only be one entry in the file for a given airline
    res.json(oneAirlineStats)
  })

  router.get('/measure/:measure', (req, res) => {
    let statistics = airlineStats.reduce((h, val) => {
      h[val.airlineCode] = val[req.params.measure]
      return h
    }, {})
    res.json(statistics)
  })

  router.get('/airline_stats', (req, res) => {
    res.json(airlineStats)
  })
}
