const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const port = process.env.PORT || 3000

app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

const apiRouter = express.Router()
app.use('/api', apiRouter)
require('../app/routes')(app, apiRouter)

app.listen(port, () => {
  console.log(`App listening on port ${port}!`)
})
